from django.urls import path

from tags.views import (
    TagListView,
    TagDetailView,
)

urlpatterns = [
    path("", TagListView.as_view(), name="tags_list"),
    path("<int:pk>/", TagDetailView.as_view, name="tag_detail"),
]
